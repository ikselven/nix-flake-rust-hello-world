{
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    mozillapkgs = {
      url = "github:mozilla/nixpkgs-mozilla";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, naersk, mozillapkgs }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";

      mozilla = pkgs.callPackage (mozillapkgs + "/package-set.nix") {};
      rust = (mozilla.rustChannelOf {
        # date only needed for nightly
        #date = "2022-03-06";
        channel = "stable";
        sha256 = "4IUZZWXHBBxcwRuQm9ekOwzc0oNqH/9NkI1ejW7KajU=";
      }).rust;

      naersk-lib = naersk.lib."${system}".override {
        cargo = rust;
        rustc = rust;
      };
    in rec {
      # `nix build`
      packages.hello-world = naersk-lib.buildPackage {
        pname = "hello-world";
        root = ./.;
      };
      defaultPackage = packages.hello-world;

      # `nix run`
      apps.hello-world = utils.lib.mkApp {
        drv = packages.hello-world;
      };
      defaultApp = apps.hello-world;

      # `nix develop`
      devShell = pkgs.mkShell {
        nativeBuildInputs = [ rust pkgs.cargo-bloat pkgs.cargo-edit pkgs.cargo-outdated ];
      };
    });
}
